import React, { Component } from "react";
class OtherItems extends Component {
  state = {
    cart: {},
  };
  addcart = (index) => {
    const { items } = this.props;
    let s1 = { ...this.state };
    s1.cart.image = items[index].image;
    s1.cart.name = items[index].name;
    s1.cart.desc = items[index].desc;
    s1.cart.code = items[index].id;
    s1.cart.type = items[index].type;
    s1.cart.quantity = 1;
    this.props.showCart(s1.cart);
    this.setState(s1);
  };
  render() {
    const { items, sizes, crusts } = this.props;
    const { order, cartIndex } = this.state;
    return (
      <React.Fragment>
        <div className="row">
          {items.map((pr, index) => {
            let { id, image, type, name, desc, veg } = pr;
            return (
              <div className="col-6 border">
                <div className="row text-center">
                  <div className="col-12">
                    <img src={image} className="img-fluid img-thumbnail" />
                    <div className="col-12">
                      <h5>{name}</h5>
                    </div>
                    <div className="col-12">
                      <p>{desc}</p>
                    </div>
                    <div className="col-12 text-center">
                      <button
                        className="btn btn-primary"
                        onClick={() => this.addcart(index)}
                      >
                        Add To Cart
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}
export default OtherItems;
