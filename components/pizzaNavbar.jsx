import React, { Component } from "react";
class PizzaNavbar extends Component {
  state = {};
  render() {
    const { onView, view } = this.props;
    let active1 = view === 1 ? "active" : "";
    let active2 = view === 2 ? "active" : "";
    let active3 = view === 3 ? "active" : "";
    let active4 = view === 4 ? "active" : "";
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-light bg-light">
          <a href="#" className="navbar-brand">
            MyFavPizza
          </a>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a
                  href="#"
                  className={"nav-link " + active1}
                  onClick={() => onView(1)}
                >
                  Veg Pizza
                </a>
              </li>
              <li className="nav-item">
                <a
                  href="#"
                  className={"nav-link " + active2}
                  onClick={() => onView(2)}
                >
                  Non-Veg Pizza
                </a>
              </li>
              <li className="nav-item">
                <a
                  href="#"
                  className={"nav-link " + active3}
                  onClick={() => onView(3)}
                >
                  Side Dishes
                </a>
              </li>
              <li className="nav-item">
                <a
                  href="#"
                  className={"nav-link " + active4}
                  onClick={() => onView(4)}
                >
                  Other Items
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}
export default PizzaNavbar;
