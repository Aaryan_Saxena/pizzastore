import React, { Component } from "react";
class VegPizza extends Component {
  state = {
    cart: {},
    cartIndex: [],
  };
  addcart = (index) => {
    let size = document.getElementById("size" + index).value;
    let crust = document.getElementById("crust" + index).value;
    console.log(size, crust);
    const { items } = this.props;
    let s1 = { ...this.state };
    s1.cart.image = items[index].image;
    s1.cart.name = items[index].name;
    s1.cart.desc = items[index].desc;
    s1.cart.code = items[index].id;
    s1.cart.type = items[index].type;
    s1.cart.size = size;
    s1.cart.crust = crust;
    s1.cart.quantity = 1;
    !size
      ? alert("Choose the Size before adding to cart")
      : !crust
      ? alert("Choose the crust before adding to cart")
      : this.props.showCart(s1.cart);
    if (size && crust) {
      s1.cartIndex.push(index);
    }

    this.setState(s1);
  };
  render() {
    const { items, sizes, crusts } = this.props;
    const { order, cartIndex } = this.state;
    return (
      <React.Fragment>
        <div className="row">
          {items.map((pr, index) => {
            let { id, image, type, name, desc, veg } = pr;
            return (
              <div className="col-6 border">
                <div className="row text-center">
                  <div className="col-12">
                    <img src={image} className="img-fluid img-thumbnail" />
                    <div className="col-12">
                      <h5>{name}</h5>
                    </div>
                    <div className="col-12">
                      <p>{desc}</p>
                    </div>
                    <div className="row">
                      <div className="col-6">
                        {this.showDD(
                          "Select Size",
                          "size" + index,
                          sizes,
                          cartIndex,
                          index
                        )}
                      </div>
                      <div className="col-6">
                        {this.showDD(
                          "Select Crust",
                          "crust" + index,
                          crusts,
                          cartIndex,
                          index
                        )}
                      </div>
                    </div>
                    <div className="col-12 text-center">
                      <button
                        className="btn btn-primary"
                        onClick={() => this.addcart(index)}
                      >
                        Add To Cart
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </React.Fragment>
    );
  }

  showDD = (valOnTop, id, arr, cartIndex, index) => {
    return (
      <React.Fragment>
        <div className="form-group mb-0">
          <select
            id={id}
            className="form-control"
            disabled={this.disabled(cartIndex, index)}
          >
            <option disabled selected value="">
              {valOnTop}
            </option>
            {arr.map((opt) => (
              <option>{opt}</option>
            ))}
          </select>
        </div>
      </React.Fragment>
    );
  };
  disabled = (cartIndex, index) => {
    const { items, cart } = this.props;
    let find = cart.findIndex((pr) => pr.code === items[index].id);
    console.log(find);
    if (cartIndex.length > 0) {
      if (find >= 0) {
        for (let i = 0; i < cartIndex.length; i++) {
          let disabled = cartIndex[i] === index ? true : false;
          if (disabled) {
            return disabled;
            break;
          }
        }
      } else {
        document.getElementById("size" + index).value = "";
        document.getElementById("crust" + index).value = "";
        return false;
      }
    } else {
      return false;
    }
  };
}
export default VegPizza;
